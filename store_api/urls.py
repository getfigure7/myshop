from django.conf.urls import url

from . import views as endpoints

urlpatterns = [
    # admin urls
    url(r'^product/create$', endpoints.create_product, name='create_product'),

    # customer urls
    url(r'^products', endpoints.product_list, name='product_list'),
    url(r'^product/(?P<uuid>[^/]+)/$', endpoints.get_product, name='get_product'),

    url(r'^purchases', endpoints.purchase_list, name='purchase_list'),
    url(r'^product/purchase/(?P<uuid>[^/]+)/$', endpoints.purchase_product, name='purchase_product'),
]
