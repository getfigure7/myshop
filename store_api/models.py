from uuid import uuid4

from django.db import models
from django.utils import timezone


class Product(models.Model):
    name = models.CharField(max_length=200, unique=True)
    product_code = models.UUIDField(unique=True, default=uuid4)
    cost = models.DecimalField(max_digits=5, decimal_places=2)
    inventory = models.IntegerField(default=0)
    create_date = models.DateTimeField(default=timezone.now)


class Purchase(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='product')
    purchase_code = models.UUIDField(unique=True, default=uuid4)
    customer_name = models.CharField(max_length=200)
    customer_phone = models.CharField(max_length=200)
    customer_email = models.CharField(max_length=200)
    cost = models.DecimalField(max_digits=5, decimal_places=2)
    create_date = models.DateTimeField(default=timezone.now)