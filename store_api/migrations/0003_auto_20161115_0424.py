# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2016-11-15 09:24
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('store_api', '0002_auto_20161114_1332'),
    ]

    operations = [
        migrations.AlterField(
            model_name='purchase',
            name='product',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='store_api.Product'),
        ),
    ]
