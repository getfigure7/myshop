from django.core import serializers
from django.http import JsonResponse, HttpResponse
from django.forms.models import model_to_dict
from django.views.decorators.http import require_GET, require_POST

from .models import Product, Purchase


@require_POST
def create_product(request):
    #  should be doing some kind of validation here and better error handling/responses.
    product = Product(name=request.POST.get('name'),
                      cost=request.POST.get('cost'),
                      inventory=request.POST.get('inventory') or 0)
    product.save()

    return JsonResponse({'success': 'Successfully created product.'})


@require_GET
def product_list(request):
    products = serializers.serialize('json', Product.objects.all())

    return HttpResponse(products, content_type='application/json')


@require_GET
def get_product(request, uuid):
    product = serializers.serialize('json', Product.objects.filter(product_code=uuid))

    return HttpResponse(product, content_type='application/json')


@require_GET
def purchase_list(request):
    purchases = Purchase.objects.select_related('product').filter(customer_email=request.GET.get('email'))

    purchase_list = []

    for purchase in purchases:
        purchase_list.append({
            'product': purchase.product.pk,
            'product_name': purchase.product.name,
            'purchase_code': purchase.purchase_code,
            'customer_name': purchase.customer_name,
            'customer_phone': purchase.customer_phone,
            'customer_email': purchase.customer_email,
            'paid': purchase.cost,
            'create_date': purchase.create_date,
        })

    return JsonResponse(purchase_list, safe=False)


@require_POST
def purchase_product(request, uuid):
    # this should .filter for inventory to be available and return errors if product is out of stock etc.
    product = Product.objects.get(product_code=uuid)

    purchase = Purchase(product=product,
                        cost=product.cost,
                        customer_name=request.POST.get('customer_name'),
                        customer_phone=request.POST.get('customer_phone'),
                        customer_email=request.POST.get('customer_email'))
    purchase.save()

    #  update inventory
    product.inventory -= 1
    product.save()

    return JsonResponse({'success': 'Successfully created purchase.'})
