from django.http import HttpResponse
from django.template import loader
from django.views.decorators.http import require_GET


@require_GET
def index(request):
    template = loader.get_template('index.html')
    context = {
        'latest_question_list': 'sdfd',
    }
    return HttpResponse(template.render(context, request))