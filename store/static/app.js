var App = React.createClass({
    componentDidMount: function () {
        $(window).on('hashchange', function () {
            MoveToHash();
        }.bind(this));

        MoveToHash();
    },
    getContentComponent: function () {
        switch (this.props.pageName) {
            case 'product':
                return <ViewProduct extended={this.props.Extended} />;
            case 'products':
                return <Products />;
            case 'purchases':
                return <Purchases />;
            default:
                return <Home />;
        }
    },
    render: function () {
        return (<div>

            <div className="container">
              <div className="header clearfix">
                <Menu pageName={this.props.pageName} />

                <h3 className="text-muted">My Shop</h3>
              </div>

              { localStorage.getItem('simulatedName') && localStorage.getItem('simulatedAuth') ? (<div className="row">Logged in as: {localStorage.getItem('simulatedName')} ({localStorage.getItem('simulatedAuth')})</div>) : null }

              {this.getContentComponent()}<br /><br />


              <footer className="footer">
                <p>&copy; 2016 My Shop</p>
              </footer>

            </div>
        </div>);
    }
});


var Menu = React.createClass({
    handleLoginClick: function (e) {
        e.preventDefault();

        var name = prompt('What is your full name?');
        var email = prompt('What is your email address? (enter the same one used for previous purchases to see purchases with that email)');
        var phone = prompt('What is your phone number?');

        if (name && email && phone) {
            localStorage.setItem('simulatedAuth', email);
            localStorage.setItem('simulatedName', name);
            localStorage.setItem('simulatedPhone', phone);

            window.location.href = '/#purchases';
        } else {
            alert('You must enter your Name, Email and Phone to login.');
        }
    },
    handleLogoutClick: function () {
        localStorage.clear();
        window.location.href = '/#home';
    },
    render: function () {
        return (<nav>
          <ul className="nav nav-pills pull-right">
            {localStorage.getItem('simulatedAuth') ? (<li className={this.props.pageName == 'purchases' ? 'active' : ''}><a href="/#purchases">My Purchases</a></li>) : (<li className={this.props.pageName == 'home' ? 'active' : ''}><a href="/#home">Home</a></li>) }
            <li className={this.props.pageName == 'products' || this.props.pageName == 'product' ? 'active' : ''}><a href="/#products">Products</a></li>
            { localStorage.getItem('simulatedAuth') ? (<li><a href="#" onClick={this.handleLogoutClick}>Logout</a></li>) : (<li><a href="#" onClick={this.handleLoginClick}>Login</a></li>)}

          </ul>
        </nav>);
    }
});


var Home = React.createClass({
    render: function () {
        return (<div>Welcome to My Shop!</div>);
    }
});

var Products = React.createClass({
    getInitialState: function () {
        return {
            isLoaded: false,
            products: null
        };
    },
    componentDidMount: function () {
        this.loadProducts();
    },
    loadProducts: function() {
        $.get('/api/products', function (data) {
            this.setState({
                isLoaded: true,
                products: data
            });
        }.bind(this));
    },
    render: function () {
        if (!this.state.isLoaded) return <div>Loading available products...</div>;
        if (!this.state.products) return <div>There currently aren't any products available.</div>;

        var products = this.state.products.map(function (product, key) {
            return (<div key={key} className="col-md-4">
                <div className="tile product">
                    <div className="pull-left">
                        <h4 className="title"><a href={"/#product/" + product.fields.product_code}>{product.fields.name}</a></h4>
                    </div>
                    <div className="pull-right">
                        <i className="fa fa-external-link" />
                    </div><br /><br />

                    <div className="center">
                        <h5>Cost ${product.fields.cost}</h5>
                        <h5>{product.fields.inventory} in stock</h5>
                    </div>
                </div>
            </div>);
        }.bind(this));

        return (<div className="row">
            <h2>Products</h2>

            {products}
        </div>);
    }
});


var ViewProduct = React.createClass({
    getInitialState: function () {
        return {
            isLoaded: false,
            isPurchasing: false,
            product: null,
            customerName: localStorage.getItem('simulatedName'),
            customerEmail: localStorage.getItem('simulatedAuth'),
            customerPhone: localStorage.getItem('simulatedPhone'),
        };
    },
    componentDidMount: function () {
        this.loadProduct();
    },
    loadProduct: function() {
        $.get('/api/product/'+this.props.extended+'/', function (data) {
            this.setState({
                isLoaded: true,
                product: data[0].fields
            });
        }.bind(this));
    },
    handlePurchaseClick: function (e) {
        e.preventDefault();

        this.setState({ isPurchasing: true });
    },
    handleInputNameChange: function (e) {
        this.setState({ customerName: e.target.value });
    },
    handleInputEmailChange: function (e) {
        this.setState({ customerEmail: e.target.value });
    },
    handleInputPhoneChange: function (e) {
        this.setState({ customerPhone: e.target.value });
    },
    handlePurchaseCompleteClick: function (e) {
        e.preventDefault();

        var postData = {
            customer_name: this.state.customerName,
            customer_email: this.state.customerEmail,
            customer_phone: this.state.customerPhone,
        };

        $.post('/api/product/purchase/'+this.state.product.product_code+'/', postData, function (data) {
            // simulate an authentication system by authorizing by email of purchaser
            localStorage.setItem('simulatedName', this.state.customerName);
            localStorage.setItem('simulatedAuth', this.state.customerEmail);
            localStorage.setItem('simulatedPhone', this.state.customerPhone);

            window.location.href = '/#purchases';
        }.bind(this));
    },
    render: function () {
        if (!this.state.isLoaded) return <div>Loading product...</div>;
        if (!this.state.product) return <div>An error occurred looking up the product. Please go back and try again.</div>;

        if (this.state.isPurchasing) return (<div className="row">
            <form onSubmit={this.handlePurchaseCompleteClick}>
              <div className="form-group">
                <label>Full Name</label>
                <input type="input" className="form-control" placeholder="John Doe" defaultValue={localStorage.getItem('simulatedName') || this.state.customerName} onChange={this.handleInputNameChange} required />
              </div>
              <div className="form-group">
                <label>Email Address</label>
                <input type="input" className="form-control" placeholder="john@doe.com" defaultValue={localStorage.getItem('simulatedAuth') || this.state.customerEmail} onChange={this.handleInputEmailChange} required />
              </div>
              <div className="form-group">
                <label>Phone Number</label>
                <input type="input" className="form-control" placeholder="555-555-5555" defaultValue={localStorage.getItem('simulatedPhone') || this.state.customerPhone} onChange={this.handleInputPhoneChange} required />
              </div>

              <input type="submit" value="Complete Purchase" className="btn btn-default" />
            </form>
        </div>);

        return (<div className="row">
            <h2>View Product: {this.state.product.name}</h2>

            {this.state.product.inventory + ' in stock'}<br /><br />

            { this.state.product.inventory == 0 ? (<div>Sorry but since this item is out of stock, you cannot purchase. Hope isn't lost, you can always try again later.</div>) : (<a href="#" onClick={this.handlePurchaseClick} className="btn btn-md btn-primary">Buy now for ${this.state.product.cost}</a>) }
        </div>);
    }
});


var Purchases = React.createClass({
    getInitialState: function () {
        return {
            isLoaded: false,
            purchases: null
        };
    },
    componentDidMount: function () {
        this.loadPurchases();
    },
    loadPurchases: function() {
        $.get('/api/purchases/?email='+localStorage.getItem('simulatedAuth'), function (data) {
            this.setState({
                isLoaded: true,
                purchases: data
            });
        }.bind(this));
    },
    render: function () {
        if (!this.state.isLoaded) return <div>Loading previous purchases...</div>;
        if (!this.state.purchases || Object.keys(this.state.purchases).length == 0) return <div>You haven't purchased anything yet.</div>;

        var purchases = this.state.purchases.map(function (purchase, key) {
            return (<div key={key} className="col-md-4">
                <div className="tile product">
                    <div className="center">
                        <h4 className="title">{purchase.product_name}</h4>

                        <h5>Paid ${purchase.paid}</h5>
                        <h5>Purchased {purchase.create_date}</h5>
                    </div>
                </div>
            </div>);
        }.bind(this));

        return (<div className="row">
            <h2>My Purchases</h2>

            {purchases}
        </div>);
    }
});


function MoveToHash() {
    var s = window.location.hash.split('?');
    var link = s[0].replace('#', '').toLowerCase();
    var page = link.split('/')[0];
    var extended = link.substring(page.length + 1);

    console.log('page:', page);

    console.log('extended', extended);

    if (page.length == 0) window.location.href = '#home';
    //if (page == 'pagename' && extended.length == 0) window.location.href = '#pagename/secondthing';

    ReactDOM.render(React.createElement(App, { pageName: page, Extended: extended }), document.getElementById('content'));
}


ReactDOM.render(React.createElement(App, { pageName: 'home', Extended: null }), document.getElementById('content'));